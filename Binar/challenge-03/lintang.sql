-- Membuat Database
CREATE DATABASE game;

-- Membuat Tabel user_game
CREATE TABLE user_game (
    id bigserial PRIMARY KEY,
    username varchar(100) NOT NULL,
    password varchar(100) NOT NULL
);

-- Membuat Tabel user_game_biodata
CREATE TABLE user_game_biodata (
    id bigserial PRIMARY KEY,
    name varchar(100) NOT NULL,
    email varchar(100) NOT NULL,
    user_game_id integer NOT NULL
);

-- Membuat Tabel user_game_history
CREATE TABLE user_game_history (
    id bigserial PRIMARY KEY,
    play_time bigint NOT NULL,
    high_skor bigint NOT NULL,
    user_game_id integer NOT NULL
);

-- Record data kedalam tabel user_game
INSERT INTO user_game (username, password) VALUES ('Lin', 'password');
INSERT INTO user_game (username, password) VALUES ('Dan', 'password');
INSERT INTO user_game (username, password) VALUES ('Prak', 'password');

-- Record data kedalam tabel user_game_biodata
INSERT INTO user_game_biodata (name, email, user_game_id) VALUES ('Lintang', 'lintang@gmail.com', 1);
INSERT INTO user_game_biodata (name, email, user_game_id) VALUES ('Dandung', 'dandung@gmail.com', 2);
INSERT INTO user_game_biodata (name, email, user_game_id) VALUES ('Prakoso', 'prakoso@gmail.com', 3);

-- Record data kedalam tabel user_game_history
INSERT INTO user_game_history (play_time, high_skor, user_game_id) VALUES (3, 81, 1);
INSERT INTO user_game_history (play_time, high_skor, user_game_id) VALUES (4, 70, 2);
INSERT INTO user_game_history (play_time, high_skor, user_game_id) VALUES (2, 90, 3);


-- Menampilkan daftar user game dan nama aslinya di database 
SELECT 
    user_game.username AS username,
    user_game_biodata.name AS nama_pemain
FROM user_game
JOIN user_game_biodata ON user_game_biodata.user_game_id = user_game.id;

-- Menampilkan peringkat terbaik di game
SELECT 
    user_game.id AS user_game_id,
    user_game.username,
    user_game_biodata.name AS nama_pemain,
    user_game_history.high_skor AS skor
FROM user_game
JOIN user_game_biodata ON user_game_biodata.user_game_id = user_game.id
JOIN user_game_history ON user_game_history.user_game_id = user_game.id
ORDER BY user_game_history.high_skor DESC;